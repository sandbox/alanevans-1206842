Description
-----------
This module adds functionality to connect a user account on your Drupal site
to a user account on Glitch.com.  The initial module only supports minimal 
functionality aside from the oauth 2 connection process, showing an additional
tab on user pages which presents the user's glitch player information obtained
from the Glitch API

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire glitch directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Create a new API key and secret at http://api.glitch.com/keys/new/ and enter
these in the configuration form at http://mysite.com/admin/config/services/glitch.
Make sure, when creating the API key, to set Auth recirect url to 
http://mysite.com/glitch/redirect . Enable all modes and scopes during API key 
creation.

4. Administer permissions at admin/people/permissions - typical configurations
allow authenticated users to connect a glitch account (it makes no sense to allow
anonymous users to do this), and grant permission to allow either all anon users
to view Glitch profiles, or just certain users. Admin permissions, logically only
to administrator-type roles.

Support
-------
